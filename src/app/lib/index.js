/**
 * @module lib
 *
 * This module exports 3rd party libraries for the application Fremja.
 *
 * @since 0.1.0
 */
export Alt from './Alt';
export modernizr from './modernizr-custom';
